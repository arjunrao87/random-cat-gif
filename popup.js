document.addEventListener('DOMContentLoaded', function() {

	var cats = document.getElementById('cats');
	cats.addEventListener('click', function() {
		window.location.href="popup.html";
	}, false);

	var twitter = document.getElementById('twitter-share');
	twitter.addEventListener('click', function () {
		var quote = encodeURIComponent("Check out Random Cat GIF! http://bit.ly/1VbaN8Z");
		openShare("https://twitter.com/home?status="+quote);
	});

	var facebook = document.getElementById('fb-share');
	facebook.addEventListener('click', function () {
		var url = encodeURIComponent("http://bit.ly/1VbaN8Z");
		var title="Check out Random Cat GIF!";
		openShare("https://www.facebook.com/sharer/sharer.php?u="+url+"&t="+title);
	});

	// 12/8/2016: Downloads dont work at the moment
	// var download = document.getElementById( 'download' );
	// download.addEventListener('click', function(){
	// 	var link = document.createElement('a');
	// 	link.href = 'images.jpg';
	// 	link.download = 'Download.jpg';
	// 	link.click();
	// });
	if (catImage.addEventListener) {
		catImage.addEventListener('error', function() {
			window.location.href="popup.html";
		});
	}
 }, false);

function openShare (url){
	var newwindow=window.open(url,'name','height=550,width=700');
	if (window.focus) {newwindow.focus()}
		return false;
}
