# RandomCatGIF
Chrome extension to see random cat GIFs

This extension will show you a random cat gif to keep you happy.

![Alt text](images/screen-shot.png?raw=true "Random Cat GIF")

![Alt text](images/screen-shot-1.png?raw=true "Random Cat GIF")
